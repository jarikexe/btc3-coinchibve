<?php


namespace App\Services\Users;


use App\Entity\User;

class Create
{
    private $encoder;
    public function __construct($request, $encoder)
    {
        $this->encoder = $encoder;
        $validation = $this->validate(json_decode($request));
        return $this->exec($validation);
    }

    private function validate($params){
        if(!$params->email || !filter_var($params->email, FILTER_VALIDATE_EMAIL) ){
            return $this->responce(['status' => '500', 'msg'=> 'email is not valid']);
        }elseif(strlen($params->password) <= 5){
            return $this->responce(['status' => '500', 'msg'=> 'password is not valid']);
        }elseif ($params->password !== $params->repeat_password){
            return $this->responce(['status' => '500', 'msg'=> 'passwords are not equal']);
        }
        return $params;
    }

    private function exec($params){
        $user = new User();

        $user->setEmail($params->email);
        var_dump($this->encoder->encodePassword($user, $params['password']));
        die();
          $user->setPasswordHash($this->encoder->encodePassword($user, $params['password']));
//          $em->persist($user);
//          $em->flush();

          die('asdf');
    }

    private function responce(array $resp_array)
    {
        return json_encode($resp_array);
    }
}