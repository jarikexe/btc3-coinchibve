<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SingInController extends AbstractController
{
    /**
     * @Route("/singin", name="sing_in")
     */
    public function index()
    {
        return $this->render('sing_in/index.html.twig', [
            'controller_name' => 'Sing in',
        ]);
    }
}
