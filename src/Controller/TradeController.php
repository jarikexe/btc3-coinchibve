<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TradeController extends AbstractController
{
    /**
     * @Route("/trade", name="trade")
     */
    public function index()
    {
        return $this->render('trade/index.html.twig', [
            'controller_name' => 'TradeController',
        ]);
    }
}
